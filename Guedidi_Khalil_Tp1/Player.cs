﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guedidi_Khalil_Tp1
{
    public class Player
    {
        private string FirstName { get; }
        private string LastName { get; }
        public string Alias { get; set; }

        public string Name => $"{FirstName} {LastName}";

        public Spaceship DefaultSpaceship { get; set; }

        public Player(string firstName, string lastName, string alias)
        {
            FirstName = FormatName(firstName);
            LastName = FormatName(lastName);
            Alias = alias;

            DefaultSpaceship = new Spaceship(100, 50);
        }

        private static string FormatName(string name)
        {
            TextInfo textInfo = new CultureInfo("fr-FR", false).TextInfo;
            return textInfo.ToTitleCase(name.ToLower());
        }

        public override string ToString()
        {
            return $"{Alias} ({Name})";
        }

        public override bool Equals(object obj)
        {
            if (obj is Player)
            {
                Player otherPlayer = obj as Player;
                return Alias.Equals(otherPlayer.Alias, StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }
    }
}
