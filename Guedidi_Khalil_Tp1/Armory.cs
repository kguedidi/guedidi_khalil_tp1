﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guedidi_Khalil_Tp1
{
    public class Armory
    {
        public List<Weapon> Weapons { get; private set; }

        public Armory()
        {
            Weapons = new List<Weapon>();
            Init();
        }

        private void Init()
        {
            Weapons.Add(new Weapon("Laser Gun", 10, 20, EWeaponType.Direct));
            Weapons.Add(new Weapon("Missile Launcher", 25, 50, EWeaponType.Explosive));
            Weapons.Add(new Weapon("Guided Missile", 15, 30, EWeaponType.Guided));
        }

        public void ViewArmory()
        {
            Console.WriteLine("L'armurerie possède les armes suivantes : ");
            foreach (var weapon in Weapons)
            {
                Console.WriteLine($"- {weapon.Name} (Type : {weapon.WeaponType}, Dégâts : {weapon.MinDamage}-{weapon.MaxDamage})");
            }
        }

        public void AddWeapon(Weapon weapon)
        {
            Weapons.Add(weapon);
        }

        public bool RemoveWeapon(string weaponName)
        {
            Weapon weaponToRemove = Weapons.Find(w => w.Name == weaponName);
            if (weaponToRemove != null)
            {
                Weapons.Remove(weaponToRemove);
                return true;
            }
            return false;
        }
    }
}
