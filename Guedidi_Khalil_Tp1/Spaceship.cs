﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guedidi_Khalil_Tp1
{
    public class Spaceship
    {
        public int MaxStructure { get; private set; }
        public int MaxShield { get; private set; }

        public int CurrentStructure { get; private set; }
        public int CurrentShield { get; private set; }

        public bool IsDestroyed => CurrentStructure <= 0;

        public List<Weapon> Weapons { get; private set; }


        public Spaceship(int maxStructure, int maxShield)
        {
            MaxStructure = maxStructure;
            MaxShield = maxShield;
            CurrentStructure = MaxStructure;
            CurrentShield = MaxShield;

            Weapons = new List<Weapon>();
        }

        public void AddWeapon(Weapon weapon)
        {
            if (Weapons.Count < 3)
            {
                Weapons.Add(weapon);
            }
            else
            {
                Console.WriteLine("L'armurerie est remplie. Retirez une arme avant d'en rajouter une autre.");
            }
        }

        public void RemoveWeapon(Weapon weapon)
        {
            Weapons.Remove(weapon);
        }

        public void ClearWeapons()
        {
            Weapons.Clear();
        }

        public void ViewWeapons()
        {
            Console.WriteLine("Les armes de ce vaisseau : ");
            foreach (var weapon in Weapons)
            {
                Console.WriteLine($"- {weapon.Name} (Type: {weapon.WeaponType}, Dégâts: {weapon.MinDamage}-{weapon.MaxDamage})");
            }
        }

        public double AverageDamages()
        {
            return Weapons.Average(w => (w.MinDamage + w.MaxDamage) / 2.0);
        }

        public void ViewShip()
        {
            Console.WriteLine($"Points de vie maximum : {MaxStructure}");
            Console.WriteLine($"Points de vie actuels : {CurrentStructure}");
            Console.WriteLine($"Points bouclier maximum : {MaxShield}");
            Console.WriteLine($"Points bouclier actuels: {CurrentShield}");
            Console.WriteLine($"Détruit : {(IsDestroyed ? "Oui" : "Non")}");

            Console.WriteLine("Armes du vaisseau:");
            if (Weapons.Count == 0)
            {
                Console.WriteLine("Aucune arme équipée.");
            }
            else
            {
                foreach (var weapon in Weapons)
                {
                    Console.WriteLine($"- {weapon.Name} (Type: {weapon.WeaponType}, Dégâts: {weapon.MinDamage}-{weapon.MaxDamage})");
                }
            }
        }
    }
}
