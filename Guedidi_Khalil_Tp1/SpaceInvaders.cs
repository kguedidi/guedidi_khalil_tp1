﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guedidi_Khalil_Tp1
{
    public class SpaceInvaders
    {
        private List<Player> players;
        private Armory armory;

        public SpaceInvaders()
        {
            Init();
        }

        public static void Main()
        {
            SpaceInvaders game = new SpaceInvaders();

            game.DisplayPlayers();

            Console.WriteLine("Liste des armes dans l'armurerie :");
            game.armory.ViewArmory();
            Console.WriteLine();

            foreach (Player player in game.players)
            {
                Console.WriteLine($"Informations sur le vaisseau du joueur {player.Alias}:");
                player.DefaultSpaceship.ViewShip();
                Console.WriteLine();
            }
        }

        private void Init()
        {
            players = new List<Player>
        {
            new Player("Alice", "Smith", "AlienDestroyer"),
            new Player("Bob", "Johnson", "StarFighter"),
            new Player("Charlie", "Brown", "GalacticHero")
        };
            armory = new Armory();
        }

        public void DisplayPlayers()
        {
            Console.WriteLine("Liste des joueurs :");
            foreach (Player player in players)
            {
                Console.WriteLine(player.ToString());
            }
        }
    }
}
